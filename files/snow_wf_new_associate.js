try { 
 var r = new sn_ws.RESTMessageV2('New associate', 'New associate');
 r.setStringParameterNoEscape('new_user_firstname', current.variables.new_user_firstname);
 r.setStringParameterNoEscape('new_user_surname', current.variables.new_user_surname);
 r.setStringParameterNoEscape('new_user_role', current.variables.new_user_role);
 r.setStringParameterNoEscape('new_user_username', current.variables.new_user_username);
 r.setStringParameterNoEscape('new_user_password', current.variables.new_user_password);
 r.setStringParameterNoEscape('new_user_email', current.variables.new_user_email);
 r.setStringParameterNoEscape('gitlab_project_name', current.variables.gitlab_project_name);
 r.setStringParameterNoEscape('instance_name', current.variables.instance_name);
 r.setStringParameterNoEscape('snow_request', current.request.number);

 var response = r.execute();
 var responseBody = response.getBody();
 var httpStatus = response.getStatusCode();
}
catch(ex) {
 var message = ex.message;
}
