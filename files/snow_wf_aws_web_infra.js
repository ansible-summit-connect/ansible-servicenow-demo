try { 
 var r = new sn_ws.RESTMessageV2('Deploy AWS web infrastructure', 'Deploy AWS web infrastructure');
 r.setStringParameterNoEscape('region', current.u_string_1);
 r.setStringParameterNoEscape('bucket', current.u_string_2);
 r.setStringParameterNoEscape('snow_change', current.number);

 var response = r.execute();
 var responseBody = response.getBody();
 var httpStatus = response.getStatusCode();
}
catch(ex) {
 var message = ex.message;
}
