try { 
 var r = new sn_ws.RESTMessageV2('New ServiceNow user', 'New ServiceNow user');
 r.setStringParameterNoEscape('new_user_firstname', current.variables.new_user_firstname);
 r.setStringParameterNoEscape('new_user_surname', current.variables.new_user_surname);
 r.setStringParameterNoEscape('new_user_email', current.variables.new_user_email);
 r.setStringParameterNoEscape('new_user_role', current.variables.new_user_role);
 r.setStringParameterNoEscape('new_user_username', current.variables.new_user_username);
 r.setStringParameterNoEscape('new_user_password', current.variables.new_user_password);
 r.setStringParameterNoEscape('snow_request', current.request.number);

 var response = r.execute();
 var responseBody = response.getBody();
 var httpStatus = response.getStatusCode();
}
catch(ex) {
 var message = ex.message;
}
