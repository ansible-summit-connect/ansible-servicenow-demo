(function executeRule(current, previous /*null when async*/ ) {
  try { 
    var r = new sn_ws.RESTMessageV2('Remediate AWS website', 'Remediate AWS website');
    r.setStringParameterNoEscape('region', current.variables.region);
    r.setStringParameterNoEscape('bucket', current.variables.bucket);
    r.setStringParameterNoEscape('repository', current.variables.repository);
    r.setStringParameterNoEscape('snow_incident', current.number);

    var response = r.execute();
    var responseBody = response.getBody();
    var httpStatus = response.getStatusCode();
  }
catch(ex) {
  var message = ex.message;
 }
})(current, previous);
