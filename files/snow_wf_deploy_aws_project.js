try { 
 var r = new sn_ws.RESTMessageV2('Deploy AWS project', 'Deploy AWS project');
 r.setStringParameterNoEscape('region', current.variables.region);
 r.setStringParameterNoEscape('bucket', current.variables.bucket);
 r.setStringParameterNoEscape('repository', current.variables.repository);
 r.setStringParameterNoEscape('snow_request', current.request.number);

 var response = r.execute();
 var responseBody = response.getBody();
 var httpStatus = response.getStatusCode();
}
catch(ex) {
 var message = ex.message;
}
